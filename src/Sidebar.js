import React, {Component} from 'react';

export default class Sidebar extends Component {
    render() {
        return (
            <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
                <div class="brand-logo">
                    <a href="index.html">
                        <img src="../assets/images/logo-icon.png" class="logo-icon" alt="logo icon" />
                        <h5 class="logo-text">Lender Calculator</h5>
                    </a>
                </div>
                <ul class="sidebar-menu do-nicescrol">
                    <li class="sidebar-header">MAIN NAVIGATION</li>
                    
                    <li>
                        <a href="calendar.html" class="waves-effect">
                            <i class="zmdi zmdi-calendar-check"></i> <span>Calculator</span>                            
                        </a>
                    </li>
                </ul>

            </div>
        );
    }
}