import React, { Component } from 'react';
import NumberFormat from 'react-number-format';

export default class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            admfee: 80000,   
            jangkaWkt: 0,
            origfee: 0,
            effrate: 0,
            investasi: 0,
            angsuran: 0,   
            totalterima : 0,
            keuntunganinvestasi: 0            
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        }, () => {
            let vinvestasi = this.state.investasi;
            let veffrate = this.state.effrate/100;
            let vjangkaWkt = this.state.jangkaWkt;

            //let vangsuran = Math.ceil((vinvestasi * (veffrate/12) * (1+(veffrate / 12))^vjangkaWkt)/((1+(veffrate/12)^vjangkaWkt-1)));
            let vangsuran = Math.ceil((vinvestasi*(veffrate/12)*(1+(veffrate/12))**vjangkaWkt)/((1+(veffrate/12))**vjangkaWkt-1));
            let vorigfee = (vjangkaWkt >= 0 && vjangkaWkt <= 6 ? (0.01 * vinvestasi) : (0.02 * vinvestasi));
            let vtotalterima = (vangsuran * vjangkaWkt);
            
            this.setState({effrate: (veffrate*100)});
            this.setState({angsuran: vangsuran});
            this.setState({origfee: vorigfee});
            this.setState({totalterima: vtotalterima});
            this.setState({keuntunganinvestasi: (vtotalterima-vinvestasi)});
        });
    }

    Calculate(){
        let vinvestasi = this.state.investasi;
        let veffrate = this.state.effrate/100;
        let vjangkaWkt = this.state.jangkaWkt;

        //let vangsuran = Math.ceil((vinvestasi * (veffrate/12) * (1+(veffrate / 12))^vjangkaWkt)/((1+(veffrate/12)^vjangkaWkt-1)));
        let vangsuran = Math.ceil((vinvestasi*(veffrate/12)*(1+(veffrate/12))**vjangkaWkt)/((1+(veffrate/12))**vjangkaWkt-1));
        let vorigfee = (vjangkaWkt >= 0 && vjangkaWkt <= 6 ? (0.01 * vinvestasi) : (0.02 * vinvestasi));
        let vtotalterima = (vangsuran * vjangkaWkt);

        this.setState({effrate: (veffrate*100)});
        this.setState({angsuran: vangsuran});
        this.setState({origfee: vorigfee});
        this.setState({totalterima: vtotalterima});
        this.setState({keuntunganinvestasi: (vtotalterima-vinvestasi)});
    }

    render() {
        let numberform = 'form-control';
        return (
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <div style={{ height: "600px" }}>
                                <div class="card">
                                    <div class="card-body">
                                        <form id="personal-info">
                                            <h4 class="form-header text-uppercase">
                                                <i class="fa fa-calculator"></i>
                                                LENDER INVESTOR PROFIT CALCULATOR
                                            </h4>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Admin Fee</label>
                                                <div class="col-sm-10">
                                                    <NumberFormat className={numberform} thousandSeparator={true} prefix={'Rp. '} value="80000" readOnly></NumberFormat>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Origination Fee</label>
                                                <div class="col-sm-10">
                                                    <NumberFormat className={numberform} thousandSeparator={true} prefix={'Rp. '} value={this.state.origfee} onChange={this.handleInputChange} readOnly></NumberFormat>                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Effective Rate</label>
                                                <div class="col-sm-10">
                                                    <NumberFormat className={numberform} thousandSeparator={true} suffix={'%'} onValueChange={(values) => { const {value} = values; this.setState({effrate: value}, this.Calculate) }} name="effectiveRate"></NumberFormat>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Investasi</label>
                                                <div class="col-sm-10">
                                                    <NumberFormat className={numberform} thousandSeparator={true} prefix={'Rp. '} onValueChange={(values) => { const {value} = values; this.setState({investasi: value}, this.Calculate) }}></NumberFormat>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Jangka Waktu</label>
                                                <div class="col-sm-10">
                                                    <select id="jangkaWkt" class="form-control" name="jangkaWkt" value={this.state.jangkaWkt} onChange={this.handleInputChange}>
                                                        <option value="0"> -- please select -- </option>
                                                        <option value="1"> 1 </option>
                                                        <option value="2"> 2 </option>
                                                        <option value="3"> 3 </option>
                                                        <option value="4"> 4 </option>
                                                        <option value="5"> 5 </option>
                                                        <option value="6"> 6 </option>
                                                        <option value="7"> 7 </option>
                                                        <option value="8"> 8 </option>
                                                        <option value="9"> 9 </option>
                                                        <option value="10"> 10 </option>
                                                        <option value="11"> 11 </option>
                                                        <option value="12"> 12 </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Angsuran</label>
                                                <div class="col-sm-10">
                                                <NumberFormat className={numberform} thousandSeparator={true} prefix={'Rp. '} value={this.state.angsuran} name="angsuran" readOnly></NumberFormat>
                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Total yang diterima</label>
                                                <div class="col-sm-10">
                                                    <NumberFormat className={numberform} thousandSeparator={true} prefix={'Rp. '} value={this.state.totalterima}  name="totalterima" readOnly></NumberFormat>                                                    
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="input-1" class="col-sm-2 col-form-label">Keuntungan Investasi</label>
                                                <div class="col-sm-10">                                                    
                                                    <NumberFormat className={numberform} thousandSeparator={true} prefix={'Rp. '} value={this.state.keuntunganinvestasi} name="keuntunganinvestasi" readOnly></NumberFormat>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-body">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Nama Product</th>
                                                    <th>Loan</th>
                                                    <th>Admin Fee</th>
                                                    <th>Portion Effective Rate</th>
                                                    <th>Origination Fee</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Product 1</td>
                                                    <td><NumberFormat thousandSeparator={true} prefix={'Rp. '} displayType={'text'} value={this.state.investasi} name="tinvestasi"></NumberFormat></td>
                                                    <td><NumberFormat displayType={'text'} thousandSeparator={true} prefix={'Rp. '} value={this.state.admfee} name="tadmfee"></NumberFormat></td>
                                                    <td><NumberFormat displayType={'text'} thousandSeparator={true} suffix={'%'} value={this.state.effrate} name="teffrate"></NumberFormat></td>
                                                    <td><NumberFormat displayType={'text'} thousandSeparator={true} prefix={'Rp. '} value={this.state.origfee} name="torigfee"></NumberFormat></td>
                                                </tr>
                                            </tbody>
                                        </table>                                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="overlay"></div>
            </div>
        );
    }
}