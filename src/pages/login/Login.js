import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Redirect } from 'react-router-dom';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username:'',
            password:'',
            isAuth: false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        document.body.className = 'bg-dark';
    }

    handleChange(event){
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
          });
    }

    handleSubmit(event) {
        event.preventDefault();
        let username = this.state.username;
        let password = this.state.password;
        let url = 'http://localhost:5000/Auth/Login';

        let formdata = new FormData();
        formdata.set('username', username);
        formdata.set('password', password);

        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
                "Access-Control-Allow-Origin": "*",
            }
          };

        axios.post(url, formdata, axiosConfig)
        .then((response) => {
            var user = response.data.collection;
            window.localStorage.setItem('user', user);
                this.setState({isAuth: true});
        })
        .catch((error) => {
            console.info(error);
            alert('Data tidak ditemukan!');
        });
    }

    render() {
        const isAuth = this.state.isAuth;
        if(isAuth){
            return <Redirect to='/'/>;
        }

        return (
            <div class="card card-authentication1 mx-auto my-5">
                <div class="card-body">
                    <div class="card-content p-2">
                        <div class="text-center">
                            <img src="assets/images/logo-icon.png" alt="logo icon" />
                        </div>
                        <div class="card-title text-uppercase text-center py-3">Sign In</div>
                        <form>
                            <div class="form-group">
                                <label for="exampleInputUsername" class="">Username</label>
                                <div class="position-relative has-icon-right">
                                    <input type="text" id="exampleInputUsername" name="username" onChange={this.handleChange} class="form-control input-shadow" placeholder="Enter Username" />
                                    <div class="form-control-position">
                                        <i class="icon-user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword" class="">Password</label>
                                <div class="position-relative has-icon-right">
                                    <input type="password" id="exampleInputPassword" name="password" onChange={this.handleChange} class="form-control input-shadow" placeholder="Enter Password" />
                                    <div class="form-control-position">
                                        <i class="icon-lock"></i>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary shadow-primary btn-block waves-effect waves-light" onClick={this.handleSubmit}>Sign In</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;