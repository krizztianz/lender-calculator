import React, { Component } from 'react';
import Header from './Header';
import Sidebar from './Sidebar';
import Content from './Content';
import Footer from './Footer';
import {
  BrowserRouter as Router,
  Redirect
} from "react-router-dom";

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
        user:'',
        isAuthenticated:false
    }
  }

  componentDidMount(){

    document.body.className = document.body.className.replace("bg-dark","");

    // Append Javascript
    this.createJS('sidebar', process.env.PUBLIC_URL + '/assets/js/sidebar-menu.js');
    this.createJS('simplebar', process.env.PUBLIC_URL + '/assets/plugins/simplebar/js/simplebar.js');
    this.createJS('waves', process.env.PUBLIC_URL + '/assets/js/waves.js');
    this.createJS('custom', process.env.PUBLIC_URL + '/assets/js/app-script.js');

    // Append CSS
    this.createCSS('sidebar', process.env.PUBLIC_URL + '/assets/css/sidebar-menu.css');
    this.createCSS('simplebar', process.env.PUBLIC_URL + '/assets/plugins/simplebar/css/simplebar.css');
  }

  createJS(id, path){
    var script = document.createElement('script');
    script.id = id+'js';
    script.src= process.env.PUBLIC_URL + path;
    document.body.appendChild(script);
  }

  createCSS(id, path){
    var style = document.createElement('link');
    style.id = id+'css';
    style.rel = 'stylesheet';;
    style.href = process.env.PUBLIC_URL + path;
    document.head.appendChild(style);
  }

  render() {
    const user = window.localStorage.getItem('user');
    let redirect = false;

    if(user === null){
      redirect = true;
    } else {
      redirect = false;
    }

    if (redirect) {
      return <Redirect to='/login'/>;
    }

    return (
      <div>
        <Header />
        <Sidebar />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default App;